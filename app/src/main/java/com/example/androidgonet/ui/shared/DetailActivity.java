package com.example.androidgonet.ui.shared;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidgonet.R;
import com.example.androidgonet.RoomData.DatabaseClient;
import com.example.androidgonet.RoomData.TblMovie;
import com.example.androidgonet.RoomData.TblRecommendations;
import com.example.androidgonet.adapter.AdapterMovie;
import com.example.androidgonet.api.RetrofitClient;
import com.example.androidgonet.model.body;
import com.example.androidgonet.model.gralResult;
import com.example.androidgonet.model.movie;
import com.example.androidgonet.model.results;
import com.example.androidgonet.model.sessionResult;
import com.example.androidgonet.model.tokenResult;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    TextView txtOverView;
    TextView txtTitle;
    RatingBar mRating;
    private RecyclerView recyclerView;
    private String id;
    private List<movie> listOfMovies=new ArrayList<movie>();
    private String apikey="37c075c1b8afbefded4c40b3debf9b29";
    private String category="popular";
    private String token;
    private AdapterMovie adapter;
    boolean stateNetwork;
    private DatabaseClient db_movie;
    private String TYPE;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        txtOverView=(TextView) findViewById(R.id.txtOverView) ;
        txtTitle=(TextView) findViewById(R.id.txtTitle) ;
        mRating=(RatingBar)findViewById(R.id.ratingBar);
        Bundle extras = getIntent().getExtras();
        String overview;
        mRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                //llamar funcion para consumir el metodo post
                postRate(v);
            }
        });

        if (extras != null) {
            overview = extras.getString("overview");
            id=extras.getString("id");
            TYPE=extras.getString("type");
            txtOverView.setText(overview);
            txtTitle.setText(extras.getString("title"));
        }

        //obtener token de acceso
        Authorize();
        //inicializar recycler para lista de recomendaciones
        initRecycler();
        //Determinar si carga se hará desde la base de datos local o usando la api
        determineCarga();
    }
    public void determineCarga(){
        ConnectivityManager cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        stateNetwork = activeNetwork != null && activeNetwork.getType()==1 && activeNetwork.isConnectedOrConnecting();
        if(stateNetwork)
            getList();
        else
            getLocal();
    }
   public void initRecycler(){
       recyclerView =(RecyclerView) findViewById(R.id.rcMovies);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));

    }
    //Obtener datos consumiendo la api
    public void getList() {

        listOfMovies = new ArrayList<movie>();
        Call<results> call = TYPE.equals("MOVIE") ? RetrofitClient.getInstance().getMyApi().getlistRecomendations(Integer.parseInt(id), apikey):
                                                    RetrofitClient.getInstance().getMyApi().getlistRecomendationsTV(Integer.parseInt(id), apikey);
        call.enqueue(new Callback<results>() {
            @Override
            public void onResponse(Call<results> call, Response<results> response) {

                if (response.isSuccessful()) {
                    results results = response.body();
                    listOfMovies = results.getResults();
                    //inicializar la instancia de la base de datos local
                    db_movie = new DatabaseClient(getApplicationContext());
                    //limpiar la tabla para actualizar los datos
                    db_movie.delete_tbl_recommendations_all(Integer.parseInt(id));
                    //inicializar tabla de recomendaciones
                    TblRecommendations mRecommendations = new TblRecommendations();
                    //Guardar datos en la base de datos local
                    for(int i = 0; i < listOfMovies.size(); i++) {
                        mRecommendations.getId_tbl();
                        mRecommendations.setId(listOfMovies.get(i).getId());
                        mRecommendations.setOriginalTitle(listOfMovies.get(i).getOriginalTitle());
                        mRecommendations.setTitle(listOfMovies.get(i).getTitle());
                        mRecommendations.setPosterPath(listOfMovies.get(i).getPosterPath());
                        mRecommendations.setIdmovie(Integer.parseInt(id));
                        db_movie.set_db_recommendations(mRecommendations);
                    }


                    adapter = new AdapterMovie(getApplicationContext(), listOfMovies, true, null);
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<results> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Ocurrió un error al obtener los datos", Toast.LENGTH_LONG).show();
            }

        });
    }
    //obtener datos de la base de datos local
    public void getLocal(){
        db_movie = new DatabaseClient(getApplicationContext());
        listOfMovies=new ArrayList<movie>();
        List<TblRecommendations> recommendations;
        recommendations = db_movie.get_list_movie(Integer.parseInt(this.id));

        for(int i=0; i  < recommendations.size(); i++) {
            TblRecommendations f = recommendations.get(i);
            movie mMovie=new movie();
            mMovie.setTitle(f.getTitle());
            mMovie.setOriginalTitle(f.getOriginalTitle());
            mMovie.setOverview(f.getOverview());
            mMovie.setId(f.getId());
            mMovie.setPopularity(f.getPopularity());
            mMovie.setPosterPath(f.getPosterPath());
            mMovie.setReleaseDate(f.getReleaseDate());
            listOfMovies.add(mMovie);
        }
        adapter = new AdapterMovie(getApplicationContext(),listOfMovies,stateNetwork, null);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter( adapter);



    }
    //obtener  token de acceso
    public void Authorize(){
        Call<tokenResult> call = RetrofitClient.getInstance().getMyApi().getGuest_session( apikey);
        call.enqueue(new Callback<tokenResult>() {
            @Override
            public void onResponse(Call<tokenResult> call, Response<tokenResult> response) {

                if (response.isSuccessful()) {
                    tokenResult res=response.body();
                    token=res.getGuestSessionId();
                }
                else{
                    Toast.makeText(getApplicationContext(), "No fue posible obtener el token", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<tokenResult> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Ocurrió un error al obtener el token de acceso", Toast.LENGTH_LONG).show();
            }
        });
    }
    //calificar movie
    public void postRate(float rating){
        body value=new body();
        value.setValue(rating);

        Call<gralResult> call = TYPE.equals("MOVIE") ?  RetrofitClient.getInstance().getMyApi().setRate(Integer.parseInt(id), apikey,token,value):
                RetrofitClient.getInstance().getMyApi().setRateTV(Integer.parseInt(id), apikey,token,value);
        call.enqueue(new Callback<gralResult>() {
            @Override
            public void onResponse(Call<gralResult> call, Response<gralResult> response) {

                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), response.body().getStatusMessage(), Toast.LENGTH_LONG).show();

                }
                else{
                    Toast.makeText(getApplicationContext(), "Movie Rated error", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<gralResult> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "No fué posible realizar la operación", Toast.LENGTH_LONG).show();
            }
        });
    }

}
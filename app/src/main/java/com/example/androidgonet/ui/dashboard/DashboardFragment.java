package com.example.androidgonet.ui.dashboard;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.androidgonet.R;
import com.example.androidgonet.RoomData.DatabaseClient;
import com.example.androidgonet.RoomData.TblMovie;
import com.example.androidgonet.adapter.AdapterMovie;
import com.example.androidgonet.api.RetrofitClient;
import com.example.androidgonet.databinding.FragmentMovieBinding;
import com.example.androidgonet.databinding.FragmentTvBinding;
import com.example.androidgonet.listener.RecyclerListener;
import com.example.androidgonet.model.movie;
import com.example.androidgonet.model.results;
import com.example.androidgonet.ui.shared.DetailActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private FragmentTvBinding binding;
    private DatabaseClient db_movie;
    List<movie> listOfMovies=new ArrayList<movie>();
    String apikey="37c075c1b8afbefded4c40b3debf9b29";
    String category="popular";
    View root;
    //inidca el estado de la conexion wifi
    boolean stateNetwork;
    // RecyclerView para mostrar el listado de elementos
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    AdapterMovie adapter;
    final String TYPE="TV";
    //Listener para escuchar al seleccionar un item de la lista
    final RecyclerListener mRecyclerListener = new RecyclerListener() {
        @Override
        public void onItemClick(int position) {
            movie item = listOfMovies.get(position);
            //Snackbar.make(view, "Selected position: " + position + "" + item.getDato(), Snackbar.LENGTH_LONG).show();
            Intent detalle=new Intent(getActivity().getApplicationContext(), DetailActivity.class);
            Bundle extras=new Bundle();
            extras.putString("id",""+item.getId());
            extras.putString("title",""+item.getName());
            extras.putString("posterPath",""+item.getPosterPath());
            extras.putString("overview",""+item.getOverview());
            extras.putString("type",""+TYPE);
            detalle.putExtras(extras);
            startActivity(detalle);

        }
    };

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        DashboardViewModel dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);

        binding = FragmentTvBinding.inflate(inflater, container, false);
        root = binding.getRoot();

        Spinner spinner = (Spinner) root.findViewById(R.id.spCategories);
        // Obtener el refreshLayout
        refreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.srMovie);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(),
                R.array.categories_array_tv, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Add adapter a spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        return root;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = root.findViewById(R.id.rcMovies);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(root.getContext(), 3));

        determineCarga();
    }
    //Determinar si la carga será local o de la api dependiendo del estado de wifi
    public void determineCarga(){
        Context context = getActivity().getApplicationContext();
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        stateNetwork = activeNetwork != null && activeNetwork.getType()==1 && activeNetwork.isConnectedOrConnecting();
        //Toast.makeText(getActivity().getApplicationContext(),"isConnected "+stateNetwork+"", Toast.LENGTH_LONG).show();

        if(stateNetwork){
            getList();
            refreshLayout();
        }
        //local
        else {
            getLocal();
            refreshLayout();
        }
    }
    //obtener datos de la base de datos local
    public void getLocal(){
        db_movie = new DatabaseClient(getActivity().getApplicationContext());
        listOfMovies=new ArrayList<movie>();
        refreshLayout.setRefreshing(true);
        List<TblMovie> Movies;
        Movies = db_movie.get_list_movie(this.category,TYPE);

        for(int i=0; i  < Movies.size(); i++) {
            TblMovie f = Movies.get(i);
            movie mMovie=new movie();
            mMovie.setTitle(f.getName());
            mMovie.setOriginalTitle(f.getOriginalTitle());
            mMovie.setOverview(f.getOverview());
            mMovie.setId(f.getId());
            mMovie.setPopularity(f.getPopularity());
            mMovie.setPosterPath(f.getPosterPath());
            mMovie.setReleaseDate(f.getReleaseDate());
            mMovie.setName(f.getName());
            listOfMovies.add(mMovie);
        }
        refreshLayout.setRefreshing(false);
        adapter = new AdapterMovie(getActivity().getApplicationContext(),listOfMovies,stateNetwork, mRecyclerListener);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter( adapter);



    }
    //obtener datos de la api
    public void getList(){
        refreshLayout.setRefreshing(true);
        listOfMovies=new ArrayList<movie>();
        Call<results> call = RetrofitClient.getInstance().getMyApi().getlistTV(category,apikey);
        call.enqueue(new Callback<results>() {
            @Override
            public void onResponse(Call<results> call, Response<results> response) {

                if (response.isSuccessful()) {
                    results results = response.body();
                    listOfMovies = results.getResults();
                    db_movie = new DatabaseClient(getActivity().getApplicationContext());
                    //limpiar la tabla para actualizar los datos
                    db_movie.delete_tbl_movie_all(category,TYPE);
                    TblMovie Movie = new TblMovie();
                    for(int i = 0; i < listOfMovies.size(); i++) {
                        Movie.setId(listOfMovies.get(i).getId());
                        Movie.setOriginalTitle(listOfMovies.get(i).getOriginalTitle());
                        Movie.setTitle(listOfMovies.get(i).getTitle());
                        Movie.setPosterPath(listOfMovies.get(i).getPosterPath());
                        Movie.setOverview(listOfMovies.get(i).getOverview());
                        Movie.setCategory(category);
                        Movie.setType(TYPE);
                        Movie.setName(listOfMovies.get(i).getName());
                        db_movie.set_db_movie(Movie);
                    }
                    adapter = new AdapterMovie(getActivity().getApplicationContext(),listOfMovies,stateNetwork, mRecyclerListener);
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter( adapter);
                }
            }


            @Override
            public void onFailure(Call<results> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(),"Ocurrió un error al obtener los datos", Toast.LENGTH_LONG).show();
            }

        });
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String value=adapterView.getItemAtPosition(i).toString().replace(" ","");
        Resources res = this.getResources();
        category= res.getString(res.getIdentifier(value, "string", this.getContext().getPackageName()));

        determineCarga();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
    private void refreshLayout(){
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.teal_200),getResources().getColor(R.color.teal_700));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                determineCarga();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
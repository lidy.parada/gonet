package com.example.androidgonet.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import com.example.androidgonet.R;
import androidx.recyclerview.widget.RecyclerView;
public class ViewHolderMovie extends RecyclerView.ViewHolder{
     public TextView view;
     public ImageView img;
    public ViewHolderMovie(@NonNull View itemView) {
        super(itemView);
        view = (TextView)itemView.findViewById(R.id.titleItem);
        img=(ImageView) itemView.findViewById(R.id.img);
    }

    /*public TextView getView(){
        return view;
    }*/
}

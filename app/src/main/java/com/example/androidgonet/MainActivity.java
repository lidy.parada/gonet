package com.example.androidgonet;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.androidgonet.RoomData.DatabaseClient;
import com.example.androidgonet.RoomData.TblMovie;
import com.example.androidgonet.api.RetrofitClient;
import com.example.androidgonet.model.movie;
import com.example.androidgonet.model.results;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.androidgonet.databinding.ActivityMainBinding;
import com.google.gson.JsonArray;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity  {

    private ActivityMainBinding binding;
    String category="popular";
    private DatabaseClient db_movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView navView = findViewById(R.id.nav_view);


        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);


        //validar conexion a internet

        //Leer Local
        db_movie = new DatabaseClient(getApplicationContext());
        //db_movie.delete_tbl_movie_all();



        /* TblMovie Movie = new TblMovie();
        Movie.setId_movie(1);
        Movie.setOriginalTitle("Hola");
        Movie.setTitle("Titulo Original");
        Movie.setPosterPath("/1g0dhYtq4irTY1GPXvft6k4YLjm.jpg");
        db_movie.set_db_movie(Movie);*/


        /*List<TblMovie> Movies;
        Movies = db_movie.get_list_movie();
        String in = String.valueOf(Movies.size());
        Log.d("TblMovie: ","count2 : " + in );

        for(int i=0; i  < Movies.size(); i++) {
            TblMovie f = Movies.get(i);
            Log.d("TblMovie: ", f.getId_movie() + "");
            Log.d("TblMovie: ", f.getTitle() + "");
            Log.d("TblMovie: ", f.getOriginalTitle() + "");
        }*/

    }



}
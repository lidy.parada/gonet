package com.example.androidgonet.api;

import com.example.androidgonet.R;
import com.example.androidgonet.model.body;
import com.example.androidgonet.model.gralResult;
import com.example.androidgonet.model.movie;
import com.example.androidgonet.model.results;
import com.example.androidgonet.model.sessionResult;
import com.example.androidgonet.model.tokenResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {
    String BASE_URL = "https://api.themoviedb.org/3/";
    /*Metodo para obtener guest_session*/
    @GET ("authentication/guest_session/new")
    Call<tokenResult> getGuest_session (
            @Query("api_key") String apiKey
    );
    /*Servicios de Movie*/
        //--obtener listado de peliculas
        @GET ("movie/{category}")
        Call<results> getlist (
                @Path("category") String category,
                        @Query("api_key") String apiKey
                        );
        //--obtener el listado de recomendaciones a partir de una pelicula
        @GET ("movie/{id}/recommendations")
        Call<results> getlistRecomendations (
                @Path("id") int id,
                @Query("api_key") String apiKey
        );
        //--Calificar un item de pelicula
        @POST("movie/{id}/rating")
        Call<gralResult> setRate (
                @Path("id") int id,
                @Query("api_key") String apiKey,
                @Query("guest_session_id") String guest_session_id,
                @Body body value
                );
    /*End Servicios de Movie*/

    /*Servicios de TVShows*/
    //--obtener listado de TVShows
    @GET ("tv/{category}")
    Call<results> getlistTV (
            @Path("category") String category,
            @Query("api_key") String apiKey
    );
    //--obtener el listado de recomendaciones a partir de TVShows
    @GET ("tv/{id}/recommendations")
    Call<results> getlistRecomendationsTV (
            @Path("id") int id,
            @Query("api_key") String apiKey
    );
    //--Calificar un item de TVShow
    @POST("tv/{id}/rating")
    Call<gralResult> setRateTV (
            @Path("id") int id,
            @Query("api_key") String apiKey,
            @Query("guest_session_id") String guest_session_id,
            @Body body value
    );
    /*End Servicios de TVShows*/
}

package com.example.androidgonet.RoomData;
import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
@Database(
        entities = {TblMovie.class,TblRecommendations.class },
        version =1, exportSchema = false
)
public abstract class DbMovie extends RoomDatabase {

    public abstract DAO_Movie getDAO();


}


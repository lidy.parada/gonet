package com.example.androidgonet.RoomData;

import android.content.Context;
import android.os.AsyncTask;
import androidx.lifecycle.LiveData;
import androidx.room.Room;

import java.util.List;



public class DatabaseClient {
    private Context mCtx;
    private static DatabaseClient mInstance;

    //our app database object
    private DbMovie appDatabase;

    public DatabaseClient(Context mCtx) {
        this.mCtx = mCtx;
        appDatabase = Room.databaseBuilder(mCtx,DbMovie.class,"DbMovie")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
    }

    public static synchronized DatabaseClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new DatabaseClient(mCtx);
        }
        return mInstance;
    }
    //Métodos de TblMovie
        //Obtener la lista de movies en tblMovie de una categoria determinada
        public List<TblMovie> get_list_movie(String Category,String type) {
            return appDatabase.getDAO().get_movie(Category,type);
        }
        //Obtener todos los itmes en TblMovie
        public List<TblMovie> get_list_movie_all(String type) {
            return appDatabase.getDAO().get_movie_all(type);
        }
        //insert en TblMovie
        public void set_db_movie(final TblMovie movie) {
            appDatabase.getDAO().insert_tbl_movie(movie);
        }
        //Eliminar todos los item s de TblMovie de una categoria determinada
        public void delete_tbl_movie_all(String Category,String type){
            new AsyncTask<Void,Void,Void>(){
                @Override
                protected Void doInBackground(Void...  voids){
                    appDatabase.getDAO().delete_movie_all( Category,type);
                    return null;
                }
            }.execute();
        }
    //End Métodos de TblMovie
    //Metodos de TblRecommendations
        //Obtener la lista de movies en TblRecommendations de una movie especifica
        public List<TblRecommendations> get_list_movie(int idMovie) {
            return appDatabase.getDAO().get_recommendations(idMovie);
         }
    //insert en TblRecommendations
    public void set_db_recommendations(final TblRecommendations recommendations) {
        appDatabase.getDAO().insert_tbl_recommendations(recommendations);
    }
    //Eliminar todos los item s de TblRecommendations de una movie especifica
    public void delete_tbl_recommendations_all(int idmovie){
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void...  voids){
                appDatabase.getDAO().delete_recommendations_all( idmovie);
                return null;
            }
        }.execute();
    }
    //End Metodos de TblRecommendations
}

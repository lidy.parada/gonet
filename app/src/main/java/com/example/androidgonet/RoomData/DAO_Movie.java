package com.example.androidgonet.RoomData;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface DAO_Movie {
    //Métodos de TblMovie
        @Query("SELECT * FROM TblMovie WHERE category = :Category and type=:type")
        List<TblMovie> get_movie(String Category,String type);

        @Query("SELECT * FROM TblMovie where type=:type ")
        List<TblMovie> get_movie_all(String type);

        @Insert
        void insert_tbl_movie(TblMovie movie);

        @Query("DELETE FROM TblMovie  WHERE category = :Category and type=:type")
        void delete_movie_all(String Category,String type);
    //End Métodos de TblMovie
    //Metodos de TblRecommendations
        @Query("SELECT * FROM TblRecommendations WHERE idmovie = :idmovie")
        List<TblRecommendations> get_recommendations(int idmovie);

        @Insert
        void insert_tbl_recommendations(TblRecommendations recommendations);

        @Query("DELETE FROM TblRecommendations  WHERE idmovie = :idmovie")
        void delete_recommendations_all(int idmovie);
    //End Metodos de TblRecommendations
}

package com.example.androidgonet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.androidgonet.R;
import com.example.androidgonet.ViewHolder.ViewHolderMovie;
import com.example.androidgonet.listener.RecyclerListener;
import com.example.androidgonet.model.movie;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterMovie extends RecyclerView.Adapter   {
   List<movie> lista;
   Context mcontext;
   boolean stateNetwork;
   String base_url_images="https://image.tmdb.org/t/p/w200";
   private final RecyclerListener mMyClickListener;
    /**
     * Initialize the dataset of the Adapter.     *
     * @param data List<movie> contiene los datos que serán presentados en el recyclerView
     * @param mMyClickListener
     */
    public AdapterMovie(Context mcontext_, List<movie> data, boolean stateNetwork_, RecyclerListener mMyClickListener) {
        this.mcontext=mcontext_;
        this.lista = data;
        this.stateNetwork=stateNetwork_;
        this.mMyClickListener = mMyClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int idView=R.layout.item;
        View view = LayoutInflater.from(parent.getContext()).inflate(idView, parent, false);

        return new ViewHolderMovie(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        movie item = lista.get(position);
        RecyclerView.ViewHolder viewHolder = holder;
        String title=item.getTitle()!=null? item.getTitle(): item.getName();
        ((ViewHolderMovie) holder).view.setText( title);
        if(stateNetwork)
        Picasso.get().load(base_url_images+item.getPosterPath()).into(((ViewHolderMovie) holder).img);
        else {
            //Si el dispositivo no cuenta con conexion wifi, se cargará la imagen de la memoria cache
            Picasso.get().load(base_url_images + item.getPosterPath()).networkPolicy(NetworkPolicy.OFFLINE).into(((ViewHolderMovie) holder).img);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get the position of this Vh
                int position = viewHolder.getAdapterPosition();
                if (mMyClickListener != null) mMyClickListener.onItemClick(position);
            }
        });
        }



    @Override
    public int getItemCount() {
        return lista.size();
    }


}
